#!/bin/bash
# script to automate pdf files from md files using the asta/stupa pandoc template

if [ "$#" == 0 ]; then  # no input given
    echo "todo print proper help message"
    echo "usage: md2pdf [-d documenttype] [-t templatename] file.md"
    echo "[-d]: document types:\n\t* beschluss\n\t* protokoll\n\t* einladung\n\t* bescheinigung"
    exit 1
fi

# read arguments

documenttype="beschluss"
template="stupa2021"

if [ "$#" == 1 ]; then
    echo "using stupa 2021 beschluss template as default"
else
    while getopts "d:t:" flag; do
        case "$flag" in
            d) documenttype=$OPTARG;;
            t) template=$OPTARG;;
        esac
    done
fi

# get and set file paths

filepath=${@:$OPTIND:1}
fullfilename=$(basename -- "$filepath")
path=$(dirname -- "$filepath")
extension="${fullfilename##*.}"
filename="${fullfilename%.*}"

mdfile=$filepath
metafile="${path}/${filename}_meta.md"
if [ ! -f "$metafile" ]; then
    echo "$metafile not found. Assuming metadata is included in $mdfile"
    metafile=""
fi
pdffile="${path}/${filename}.pdf"

# copy template pdf files to target directory

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

TEMPLATE=("${DIR}"/AStA-Protokolle-Background*pdf)
for f in "${TEMPLATE[@]}"
do
    cp ${f} "${path}/"
done

# call pandoc

if [ "$documenttype" == 'beschluss' ]; then
    pandoc --template $template -V lang=de "${mdfile}" "${metafile}" -o "${pdffile}"
elif [ "$documenttype" == 'protokoll' ]; then
    pandoc --template $template -V lang=de "${mdfile}" "${metafile}" --toc --number-sections --toc-depth=2 -o "${pdffile}"
elif [ "$documenttype" == 'einladung' ]; then
    pandoc --template $template -V lang=de "${mdfile}" "${metafile}" --toc --number-sections --toc-depth=2 -o "${pdffile}"
elif [ "$documenttype" == 'bescheinigung' ]; then
    pandoc --template $template -V lang=de "${mdfile}" "${metafile}" -o "${pdffile}"
else
    echo "documenttype $documenttype not existing. use one existing instead. todo wirte existing ones here"
    exit 1
fi

echo "If no errors occured during pandoc call, generated file to ${pdffile}."
