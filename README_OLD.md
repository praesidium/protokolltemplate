# protokolltemplate

![Logo](asta_logo_rgb.png)

Current Version: initial commit

## Content

0. [Content](#content)
1. [Installation](#installation)
2. [Usage](#usage)
3. [License](#license)
4. [Known issues](#known-issues)

## Installation

### Dependecies
* pandoc

### Setup
1. Clone the repository

```
git clone https://gitlab.gwdg.de/dnd-referat/protokolltemplate.git
```

2. Move *todo* to your latex template folder
```
mv asta2020.tex ~/.local/share/pandoc/templates/
```

## Usage

```
pandoc protokoll.md -o protokoll.pdf --template asta2020 -V lang=de
```

## License

Beerware for now. Will be a different open source licence in the future.

> ----------------------------------------------------------------------------  
> "THE BEER-WARE LICENSE" (Revision 42):  
> <feconi@posteo.de> wrote this thing. As long as you retain this notice you  
> can do whatever you want with this stuff. If we meet some day, and you think  
> this stuff is worth it, you can buy me a beer in return - Astarix  
> ----------------------------------------------------------------------------  

## Known issues

* template not yet written
